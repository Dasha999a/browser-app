package com.example.browserapp

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.android.material.textfield.TextInputEditText


class MainActivity : AppCompatActivity() {
    lateinit var searchButton : Button
    lateinit var webView : WebView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        searchButton = findViewById(R.id.button)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = getString(R.string.app_name)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.black))
        setSupportActionBar(toolbar)

        webView = findViewById(R.id.webView)
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }
        }

        webView.webChromeClient = object : WebChromeClient() {
            override fun onReceivedTitle(view: WebView?, title: String?) {
                super.onReceivedTitle(view, title)
                if (title?.split("-")?.get(0)?.trim()?.length != 0) {
                    toolbar.title = title
                } else {
                    toolbar.title = getString(R.string.app_name)
                }
            }
        }
        val editText = findViewById<TextInputEditText>(R.id.textInputEditText)

        searchButton.setOnClickListener {
            webView.loadUrl(editText.text.toString())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val color = when (item.itemId) {
            R.id.green -> R.color.green
            R.id.blue -> R.color.blue
            R.id.violet -> R.color.purple_700
            R.id.brown ->  R.color.brown
            R.id.yellow -> R.color.yellow
            R.id.orange -> R.color.orange
            else -> R.color.purple_700
        }

        searchButton.setBackgroundColor(ContextCompat.getColor(this,color))

        return super.onOptionsItemSelected(item)
    }
}